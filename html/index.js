
var server = ['https://janus.ericbetts.org/janus'];
var janus = null;
var sfutest = null;
var opaqueId = "videoroomtest-"+Janus.randomString(12);

var started = false;

var myid = null;
var mystream = null;
// We use this other ID just to map our subscriptions to us
var mypvtid = null;

var feeds = [];

function onStart() {
	if(started)
		return;
	started = true;
	// Make sure the browser supports WebRTC
	if(!Janus.isWebrtcSupported()) {
		bootbox.alert("No WebRTC support... ");
		return;
	}
	// Create session
	janus = new Janus({
		server: server,
		success: successCallback,
		error: errorCallback,
		destroyed: function() {
			window.location.reload();
		}
	});
}

function errorCallback(error) {
	Janus.error(error);
	bootbox.alert(error, function() {
		window.location.reload();
	});
}

function successCallback() {
	// Attach to video room test plugin
	janus.attach({
		plugin: "janus.plugin.videoroom",
		opaqueId: opaqueId,
		success: function(pluginHandle) {
			sfutest = pluginHandle;
			registerUsername();
		},
		error: function(error) {
			Janus.error("  -- Error attaching plugin...", error);
			bootbox.alert("Error attaching plugin... " + error);
		},
		webrtcState: function(on) {
			Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
		},
		ondataopen: function() {
			Janus.log("Local: a Data Channel is available and ready to be used");
		},
		ondata: function() {
			Janus.log("Local: data has been received through the Data Channel");
		},
		onmessage: onLocalMessage,
		onlocalstream: function(stream) {
			mystream = stream;
			Janus.attachMediaStream($('#myvideo').get(0), stream);
			var videoTracks = stream.getVideoTracks();
		},
		onremotestream: function(stream) {
			Janus.log("onremotestream");
			// The publisher stream is sendonly, we don't expect anything here
		},
		oncleanup: function() {
			Janus.log(" ::: Got a cleanup notification: we are unpublished now :::");
			mystream = null;
			$('#publish').text('Publish');
			$('#publish').click(function() { publishOwnFeed(true); });
		}
	});
}


function onLocalMessage(msg, jsep) {
	console.log("onLocalMessage", msg);
	var event = msg["videoroom"];
	Janus.debug(" ::: Got a message (publisher) :::", event);
	Janus.debug("Event: " + event);
	if(event != undefined && event != null) {
		if(event === "joined") {
			// Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
			myid = msg["id"];
			mypvtid = msg["private_id"];
			publishOwnFeed(true);
			// Any new feed to attach to?
			if(msg["publishers"] !== undefined && msg["publishers"] !== null) {
				var list = msg["publishers"];
				for(var f in list) {
					var id = list[f]["id"];
					var display = list[f]["display"];
					newRemoteFeed(id, display)
				}
			}
		} else if(event === "destroyed") {
			// The room has been destroyed
			Janus.warn("The room has been destroyed!");
			bootbox.alert("The room has been destroyed", function() {
				window.location.reload();
			});
		} else if(event === "event") {
			// Any new feed to attach to?
			if(msg["publishers"] !== undefined && msg["publishers"] !== null) {
				var list = msg["publishers"];
				for(var f in list) {
					var id = list[f]["id"];
					var display = list[f]["display"];
					newRemoteFeed(id, display)
				}
			} else if(msg["leaving"] !== undefined && msg["leaving"] !== null) {
				// One of the publishers has gone away?
				var leaving = msg["leaving"];
				Janus.log("Publisher leaving: " + leaving);
				var remoteFeed = null;
				for(var i=1; i<6; i++) {
					if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == leaving) {
						remoteFeed = feeds[i];
						break;
					}
				}
				if(remoteFeed != null) {
					$('#remote'+remoteFeed.rfindex).empty().hide();
					$('#videoremote'+remoteFeed.rfindex).empty();
					feeds[remoteFeed.rfindex] = null;
					remoteFeed.detach();
				}
			} else if(msg["unpublished"] !== undefined && msg["unpublished"] !== null) {
				// One of the publishers has unpublished?
				var unpublished = msg["unpublished"];
				Janus.log("Publisher unpublished: " + unpublished);
				if(unpublished === 'ok') {
					// That's us
					sfutest.hangup();
					return;
				}
				var remoteFeed = null;
				for(var i=1; i<6; i++) {
					if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == unpublished) {
						remoteFeed = feeds[i];
						break;
					}
				}
				if(remoteFeed != null) {
					$('#remote'+remoteFeed.rfindex).empty().hide();
					$('#videoremote'+remoteFeed.rfindex).empty();
					feeds[remoteFeed.rfindex] = null;
					remoteFeed.detach();
				}
			} else if(msg["error"] !== undefined && msg["error"] !== null) {
				bootbox.alert(msg["error"]);
			} else {
				Janus.warn("Event, but ran out of interesting keys", msg);
			}
		} else {
			Janus.warn("Unknown event type", event);
		}
	}
	if(jsep) {
		sfutest.handleRemoteJsep({jsep: jsep});
	}
}

function onDocumentReady() {
	// Initialize the library (all console debuggers enabled)
	Janus.init({debug: ["log", "warn"], callback: onStart});
}

$(document).ready(onDocumentReady);

function registerUsername() {
	var username = 'You';
	var register = { "request": "join", "room": 1234, "ptype": "publisher", "display": username };
	sfutest.send({"message": register});
}

function publishOwnFeed(useAudio) {
	console.log("publishOwnFeed", useAudio);
	// Publish our stream
	sfutest.createOffer({
		media: { data: true, audioRecv: false, videoRecv: false, audioSend: useAudio, videoSend: true},	// Publishers are sendonly
		success: function(jsep) {
			Janus.debug("Got publisher SDP!");
			var publish = { "request": "configure", "audio": useAudio, "video": true };
			sfutest.send({"message": publish, "jsep": jsep});
		},
		error: function(error) {
			Janus.error("WebRTC error:", error);
			if (useAudio) {
				publishOwnFeed(false);
			} else {
				bootbox.alert("WebRTC error... " + JSON.stringify(error));
			}
		}
	});
}

function toggleMute() {
	var muted = sfutest.isAudioMuted();
	Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
	if(muted)
		sfutest.unmuteAudio();
	else
		sfutest.muteAudio();
	muted = sfutest.isAudioMuted();
	$('#mute').html(muted ? "Unmute" : "Mute");
}

function unpublishOwnFeed() {
	// Unpublish our stream
	$('#unpublish').attr('disabled', true).unbind('click');
	var unpublish = { "request": "unpublish" };
	sfutest.send({"message": unpublish});
}

function newRemoteFeed(id, display) {
	if (display === 'macyps' || display === 'dummy') {
		return;
	}
	// A new feed has been published, create a new plugin handle and attach to it as a listener
	var remoteFeed = null;
	janus.attach({
		plugin: "janus.plugin.videoroom",
		opaqueId: opaqueId,
		success: function(pluginHandle) {
			remoteFeed = pluginHandle;
			// We wait for the plugin to send us an offer
			var listen = { "request": "join", "room": 1234, "ptype": "listener", "feed": id, "private_id": mypvtid };
			remoteFeed.send({"message": listen});
		},
		error: function(error) {
			Janus.error("  -- Error attaching plugin...", error);
			bootbox.alert("Error attaching plugin... " + error);
		},
		onmessage: function(msg, jsep) {
			var event = msg["videoroom"];
			Janus.debug(" ::: Got a message (listener) :::", event);

			if(event != undefined && event != null) {
				if(event === "attached") {
					// Subscriber created and attached
					for(var i=1;i<6;i++) {
						if(feeds[i] === undefined || feeds[i] === null) {
							feeds[i] = remoteFeed;
							remoteFeed.rfindex = i;
							break;
						}
					}
					remoteFeed.rfid = msg["id"];
					remoteFeed.rfdisplay = msg["display"];
					if(remoteFeed.spinner === undefined || remoteFeed.spinner === null) {
						var target = document.getElementById('videoremote'+remoteFeed.rfindex);
						remoteFeed.spinner = new Spinner({top:100}).spin(target);
					} else {
						remoteFeed.spinner.spin();
					}
					Janus.log("Successfully attached to feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") in room " + msg["room"]);
					$('#remote'+remoteFeed.rfindex).removeClass('hide').html(remoteFeed.rfdisplay).show();
				} else if(msg["error"] !== undefined && msg["error"] !== null) {
					bootbox.alert(msg["error"]);
				} else if(event === "slow_link") {
          //Ignore
				} else {
					// What has just happened?
					Janus.debug("Unknown event type", event);
				}
			}
			if(jsep !== undefined && jsep !== null) {
				// Answer and attach
				remoteFeed.createAnswer(
					{
						jsep: jsep,
						media: { audioSend: false, videoSend: false },	// We want recvonly audio/video
						success: function(jsep) {
							var body = { "request": "start", "room": 1234 };
							remoteFeed.send({"message": body, "jsep": jsep});
						},
						error: function(error) {
							Janus.error("WebRTC error:", error);
							bootbox.alert("WebRTC error... " + JSON.stringify(error));
						}
					});
			}
		},
		webrtcState: function(on) {
			Janus.log("Janus says this WebRTC PeerConnection (feed #" + remoteFeed.rfindex + ") is " + (on ? "up" : "down") + " now");
		},
		ondataopen: function() {
			Janus.log("Remote: a Data Channel is available and ready to be used");
		},
		ondata: function() {
			Janus.log("Remote: data has been received through the Data Channel");
		},
		onlocalstream: function(stream) {
			Janus.log("onlocalstream");
			// The subscriber stream is recvonly, we don't expect anything here
		},
		onremotestream: function(stream) {
			Janus.debug("Remote feed #" + remoteFeed.rfindex);
			if($('#remotevideo'+remoteFeed.rfindex).length === 0) {
				// No remote video yet
				$('#videoremote'+remoteFeed.rfindex).append('<video class="rounded" id="waitingvideo' + remoteFeed.rfindex + '" width=320 height=240 />');
				$('#videoremote'+remoteFeed.rfindex).append('<video class="rounded hide" id="remotevideo' + remoteFeed.rfindex + '" autoplay/>');
			}
			// Show the video, hide the spinner and show the resolution when we get a playing event
			$("#remotevideo"+remoteFeed.rfindex).bind("playing", function () {
				if(remoteFeed.spinner !== undefined && remoteFeed.spinner !== null)
					remoteFeed.spinner.stop();
				remoteFeed.spinner = null;
				$('#waitingvideo'+remoteFeed.rfindex).remove();
				$('#remotevideo'+remoteFeed.rfindex).removeClass('hide');
			});
			$('.no-video-container').hide();
			Janus.attachMediaStream($('#remotevideo'+remoteFeed.rfindex).get(0), stream);
			var videoTracks = stream.getVideoTracks();
			if(videoTracks === null || videoTracks === undefined || videoTracks.length === 0 || videoTracks[0].muted) {
				// No remote video
				$('#remotevideo'+remoteFeed.rfindex).hide();
				$('#videoremote'+remoteFeed.rfindex).append(
					'<div class="no-video-container">' +
						'<i class="fa fa-video-camera fa-5 no-video-icon" style="height: 100%;"></i>' +
						'<span class="no-video-text" style="font-size: 16px;">No remote video available</span>' +
				'</div>');
			}
		},
		oncleanup: function() {
			Janus.log(" ::: Got a cleanup notification (remote feed " + id + ") :::");
			if(remoteFeed.spinner !== undefined && remoteFeed.spinner !== null) {
				remoteFeed.spinner.stop();
			}
			remoteFeed.spinner = null;
			$('#waitingvideo'+remoteFeed.rfindex).remove();
		}
	});
}
